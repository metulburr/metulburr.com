import os
def freespace(p):
	s = os.statvfs(p)
	return s.f_bsize * s.f_bavail
	
bytes_ = freespace('/')
KB = bytes_ / 1024
MB = (bytes_ / 1024) / 1024
GB = ((bytes_ / 1024) / 1024) / 1024

print('bytes: {}'.format(bytes_))
print('KB: {:.2f}'.format(KB))
print('MB: {:.2f}'.format(MB))
print('GB: {:.2f}'.format(GB))

