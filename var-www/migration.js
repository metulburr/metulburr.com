var l10n_errors = {
    'de-de': {
        'blog': {
            'title': "Wir sind bald wieder da.",
            'line_1': "Unsere Techniker führen gerade geplante Wartungsarbeiten durch.",
            'line_2': "Wir entschuldigen uns für die Unannehmlichkeiten."
        },
        'dash': {
            'title': "Wir sind bald wieder da!",
            'line_1': "Liebe Erdbewohner…",
            'line_2': "Wir führen gerade geplante Wartungsarbeiten durch, sind aber bald wieder da! Wir entschuldigen uns für die Unannehmlichkeiten.",
            'line_3': "Bis bald!"
        }
    },
    
    'es-es': {
        'blog': {
            'title': "Volveremos.",
            'line_1': "Nuestros ingenieros están realizando un mantenimiento programado.",
            'line_2': "Sentimos las molestias."
        },
        'dash': {
            'title': "¡Volveremos!",
            'line_1': "Atención, terrícolas…",
            'line_2': "Estamos realizando un mantenimiento programado, ¡pero volveremos en breve! Sentimos las molestias.",
            'line_3': "¡Nos vemos pronto!"
        }
    },
    
    'fr-fr': {
        'blog': {
            'title': "Nous revenons.",
            'line_1': "Nos équipes techniques procèdent actuellement à une maintenance de nos serveurs.",
            'line_2': "Nous nous excusons pour le désagrément occasionné, et vous remercions de votre patience. "
        },
        'dash': {
            'title': "Nous revenons !",
            'line_1': "Amis Terriens…",
            'line_2': "Nous procédons actuellement à une maintenance de nos serveurs, mais ne vous inquiétez pas, nous revenons aussi vite que possible.",
            'line_3': "Merci de votre patience et à très bientôt !"
        }
    },
    
    'it-it': {
        'blog': {
            'title': "Torneremo.",
            'line_1': "Al momento i nostri ingegneri stanno eseguendo la manutenzione programmata.",
            'line_2': "Ci scusiamo per eventuali disagi."
        },
        'dash': {
            'title': "Torneremo!",
            'line_1': "Popolo della Terra…",
            'line_2': "Al momento stiamo eseguendo la manutenzione programmata, ma torneremo presto! Ci scusiamo per eventuali disagi.",
            'line_3': "A presto!"
        }
    },
    
    'ja-jp': {
        'blog': {
            'title': "すぐに戻ってきます。",
            'line_1': "ただいま定期メンテナンス中です。",
            'line_2': "ご迷惑をおかけして大変申し訳ありません。"
        },
        'dash': {
            'title': "すぐに戻ってきます！",
            'line_1': "地球の皆様…",
            'line_2': "ただいま定期メンテナンス中ですが、すぐに戻ってきます！ ご迷惑をおかけいたしますが、今しばらくお待ちください。",
            'line_3': "またすぐに会いましょう！"
        }
    },
    
    'nl-nl': {
        'blog': {
            'title': "We zijn straks weer terug.",
            'line_1': "Onze technici zijn op dit moment bezig met gepland onderhoud.",
            'line_2': "Sorry voor het ongemak."
        },
        'dash': {
            'title': "We zijn zo terug!",
            'line_1': "Aardlingen…",
            'line_2': "We zijn op dit moment bezig met gepland onderhoud, maar we zijn zo weer terug! Sorry voor het ongemak.",
            'line_3': "Tot straks!"
        }
    },
    
    'pl-pl': {
        'blog': {
            'title': "Zaraz wracamy.",
            'line_1': "Nasi informatycy przeprowadzają właśnie zaplanowane prace serwisowe na naszych serwerach",
            'line_2': "Przepraszamy za tą niedogodność."
        },
        'dash': {
            'title': "Zaraz wracamy!",
            'line_1': "Ziemianie…",
            'line_2': "Przeprowadzamy właśnie zaplanowane prace serwisowe, lecz już wkrótce wszystko wróci do normy! Przepraszamy za tą niedogodność.",
            'line_3': "Do zobaczenia!"
        }
    },
    
    'pt-br': {
        'blog': {
            'title': "Voltaremos em breve.",
            'line_1': "Nossos engenheiros estão executando uma manutenção programada.",
            'line_2': "Pedimos desculpas por qualquer inconveniência."
        },
        'dash': {
            'title': "Voltaremos em breve!",
            'line_1': "Terráqueos…",
            'line_2': "No momento estamos executando uma manutenção programada, mas voltaremos em breve! Pedimos desculpas por qualquer inconveniência.",
            'line_3': "Até logo!"
        }
    },
    
    'pt-pt': {
        'blog': {
            'title': "Já voltamos.",
            'line_1': "Os nossos técnicos estão de momento a realizar uma manutenção no sistema.",
            'line_2': "Lamentamos o incómodo."
        },
        'dash': {
            'title': "Já voltamos!",
            'line_1': "Pessoas do Planeta Terra…",
            'line_2': "De momento, estamos a realizar uma manutenção no sistema, mas voltamos daqui a pouco. Lamentamos o incómodo.",
            'line_3': "Até já!"
        }
    },
    
    'ru-ru': {
        'blog': {
            'title': "Мы скоро вернемся!",
            'line_1': "Наши инженеры выполняют плановое обслуживание.",
            'line_2': "Приносим извинения за неудобства."
        },
        'dash': {
            'title': "Мы скоро вернемся!",
            'line_1': "Земляне…",
            'line_2': "мы сейчас выполняем плановое обслуживание, но скоро вернемся! Приносим извинения за неудобства.",
            'line_3': "До скорого!"
        }
    },
    
    'tr-tr': {
        'blog': {
            'title': "Geri döneceğiz.",
            'line_1': "Mühendislerimiz şu anda, planlı bakım çalışmasını yürütmektedir.",
            'line_2': "Rahatsızlık için bağışlayın."
        },
        'dash': {
            'title': "Geri döneceğiz!",
            'line_1': "Dünyalı Dostlarımız…",
            'line_2': "Şu anda, planlı bakım çalışmamızı yürütmekteyiz fakat en kısa zamanda geri döneceğiz! Rahatsızlık için bağışlayın.",
            'line_3': "Görüşmek üzere!"
        }
    },
};

function set_language(language) {
    if (! language || ! status_code) return;
    
    if (language && status_code && l10n_errors[language] && l10n_errors[language][status_code]) {
        for (var key in l10n_errors[language][status_code]) {
            var string = l10n_errors[language][status_code][key];
            var element = document.getElementById('l10n_' + key);
            
            if (element) element.innerHTML = string;
            if (key == 'title') document.title = string;
        }
    }
}

set_language(window.navigator.userLanguage || window.navigator.language);

// log error to google analytics
var status_code = status_code || '???';

var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-97144-8']);
_gaq.push(['_trackPageview', "/error"]);

(function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
