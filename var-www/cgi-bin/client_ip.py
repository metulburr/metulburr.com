#!/usr/bin/env python3

import cgi
import os
import time
import socket


print("Content-type: text/html")
print ("")
popup = '''
Content-type: text/html
\n
<script type="text/javascript">
function newPopup(url){
	popupwin = window.open(
url,'popUpWindow','height=700,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}

</script>
<a href="JavaScript:newPopup('/var/www/index.html');">Open a popup window</a>

'''

ip = cgi.escape(os.environ["REMOTE_ADDR"])
print('IP: {}'.format(ip))
print('<br />')
print('User-Agent: {}'.format(os.environ['HTTP_USER_AGENT']))
print('<br />')
print('{}'.format(socket.gethostbyaddr(socket.gethostname())))
print('<br /><br/ >')
s = ''
for k,v in os.environ.items():
    s += '{}:{}'.format(k,v)
    s +='<br />'
print(s)
#print('{}'.format(os.environ))
writer = open('/var/www/ip_ofclicks.txt','a')
writer.write('{} clicked link at {}\n'.format(ip, time.asctime( time.localtime(time.time()) )
))
writer.close()
