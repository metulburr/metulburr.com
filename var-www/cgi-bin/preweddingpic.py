#!/usr/bin/env python3
import cgi, os
#import cgitb; cgitb.enable()


videotag = '''
<video  width="360" height="300" controls="controls">
  <source src="../wedding_photos/{filename}" type="video/{ext}">Your browser does not support the video tag.
</video>
<br /><br /> <br />
'''
video_ext = ['.mpg','.mp4', '.flv', '.avi', '.mts', '.mpeg', '.wmv', '.mov', 'divx', 'mpeg4','.ogg', '.ogm', '.ogv', '.swf', '.vob', '.qt', '.wma', '.wmx', '.ra', '.rm', '.ram', '.rmvb', '.3gp', '.mkv']






path = '/var/www/pre_wedding_album'
#pic_list = []
pic_string = ''
filenum = len(os.listdir(path))
if not os.listdir(path):
	pic_string = 'Nothing has been uploaded yet.'
for f in os.listdir(path):
	if os.path.splitext(f)[1].lower() in video_ext:
		ext_ = os.path.splitext(f)[1][1:]
		filetag = '{}'.format(videotag.format(filename=f, ext=ext_))
	else:	
	
		filetag = '<img style="max-width:750px;max-height-750px" src="http://www.metulburr.com/wedding_album/{}"><br /><br /><br />'.format(f)
	#pic_list.append(filetag)
	#pic_string += os.stat(f).st_mtime
	pic_string += filetag

	

html = '''Content-type: text/html\n

<html>
<title>Wedding Pictures</title>

<body>
<a href="http://www.metulburr.com/wedding.html">Return</a> <br />
<center>
<b>Unable to view the videos?</b> Try the <a href="http://www.metulburr.com/wedding_album/">Index<a/>.
</center>
<center><h1>Micah and Sondra's Wedding Album</h1><center>
<center>

<b>Total:</b> {filenum}<br /><br /><br />
{pics}
</center>
</body>
</html>
'''.format(pics=pic_string, filenum=filenum)

print(html)
