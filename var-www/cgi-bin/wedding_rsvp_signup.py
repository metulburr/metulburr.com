#!/usr/bin/env python3

import cgi
import cgitb
cgitb.enable()

style_ = '''
body{
    background-image:url(../image/wed_pic.png);
    background-attachment:fixed;
    background-position:center;
    background-repeat:no-repeat;

    /*cover entire screen regardless of size of window*/
    -moz-background-size: cover;
    -webkit-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}
'''

body = '''

<h2>RSVP Online</h2>

<form action="wedding_rsvpDISABLED.py">
    First name: <input type="text" name="fname"><br>
    Last name: <input type="text" name="lname"><br>

    <br />

    Total people in your party attending: <select name="total">
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
    <option value="6">6</option>
    <option value="7">7</option>
    <option value="8">8</option>
    <option value="9">9</option>
    </select>

    <br />
    <br />

    Attending: <select name="attend">
    <option>Wedding and Reception</option>
    <option>Wedding</option>
    <option>Reception</option>
    </select>

    <br /> <br />

    <input type="submit" value="Disabled">
</form>
'''

html = """Content-type: text/html\n

<html>
    <style>{STYLE}</style>
	<head>
		<title>{TITLE}</title>
		{CSS}
		<script></script>
	</head>
<body>
{BODY}
</body>
</html>
""".format(
	TITLE = 'RSVP Signup',
	CSS = '', #css links
	BR = 'br />',
	COM = '<!--{}-->'.format(''),
	SPACE = '&nbsp;',
	TAB = '&nbsp;' * 4,
	QUOTE = '&quot;', #double quotes
	LEFT = '&lt;', #left carrot
	RIGHT = '&gt;', #right carrot
    STYLE=style_,
	BODY=body
	)
print(html)
