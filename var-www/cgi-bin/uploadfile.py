#!/usr/bin/env python3
import cgi, os
import cgitb; cgitb.enable()

style_ = '''
body{
    background-image:url(../image/wed_pic.png);
    background-attachment:fixed;
    background-position:center;
    background-repeat:no-repeat;

    /*cover entire screen regardless of size of window*/
    -moz-background-size: cover;
    -webkit-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}
'''

try:
	import msvcrt
	msvcrt.setmode (0, os.O_BINARY)
	msvcrt.setmode (1, os.O_BINARY)
except ImportError:
	pass

form = cgi.FieldStorage()
message = ''
try:
	try:
	#	for upload in form:
		for upload in form['userfile']:
	#		upload = form['userfile']
			if upload.filename:
				name = os.path.basename(upload.filename)
				out = open('/var/www/wedding_album/' + name, 'wb', 1000)
				filepath = "http://www.metulburr.com/wedding_album/{}".format(name)
				message += "The file '" + name + "' was uploaded successfully<br>"
			else:
				#message += "There was an error in uploading the file {}<br>".format(name)
				pass
			while True:
				packet = upload.file.read(1000)
				if not packet:
					break

				out.write(packet)
			out.close()

	except TypeError:
	#	message = 'Keyerror'
			upload = form['userfile']
			if upload.filename:
				name = os.path.basename(upload.filename)
				out = open('/var/www/wedding_album/' + name, 'wb', 1000)
				filepath = "http://www.metulburr.com/wedding_album/{}".format(name)
				message += "The file '" + name + "' was uploaded successfully<br>"
			else:
				#message += "There was an error in uploading the file {}<br>".format(name)
				pass
			while True:
				packet = upload.file.read(1000)
				if not packet:
					break

				out.write(packet)
			out.close()
except NameError: #user left field blank
	filepath = ''
	message = 'You did not submit anything! <br> Click Browse to select files'



html = '''Content-type: text/html\n

<html>
<title>Wedding Pictures</title>
<style>{STYLE}</style>

<body>
<center>
<a href="http://www.metulburr.com/wedding.html">Return</a>
<h3>{msg}</h3>
Thank you for participating in our wedding. <br /><br />
<!--<a href="{filepath}">{filepath}</a>-->
<a href="http://www.metulburr.com/cgi-bin/weddingpics.py">View Wedding Album</a>
</center>

</body>
</html>
'''.format(msg=message, filepath=filepath, STYLE=style_)
#html.format(msg=message, filepath=filepath)
print(html)
