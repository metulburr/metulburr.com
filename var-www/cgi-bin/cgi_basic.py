#!/usr/bin/env python3


html = """Content-type: text/html\n

<html>
	<head>
		<title>{TITLE}</title>
		{CSS}
		<script></script>
	</head>
<body>
{TAB}{LEFT}br{RIGHT}
</body>
</html>
""".format(
	TITLE = 'CGI test',
	CSS = '', #css links
	BR = 'br />',
	COM = '<!--{}-->'.format(''),
	SPACE = '&nbsp;',
	TAB = '&nbsp;' * 4,
	QUOTE = '&quot;', #double quotes
	LEFT = '&lt;', #left carrot
	RIGHT = '&gt;', #right carrot
	BODY=''
	)
print(html)
