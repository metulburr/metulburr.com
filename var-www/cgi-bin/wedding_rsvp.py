#!/usr/bin/env python3

import cgi
import os
import json
import time
import datetime

import cgitb
cgitb.enable()

style_ = '''
body{
    background-image:url(../image/wed_pic.png);
    background-attachment:fixed;
    background-position:center;
    background-repeat:no-repeat;

    /*cover entire screen regardless of size of window*/
    -moz-background-size: cover;
    -webkit-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}
'''



fs = cgi.FieldStorage()
try:
    lastname = fs['lname'].value
except KeyError:
    lastname = ''

try:
    firstname = fs['fname'].value
except KeyError:
    firstname = ''

try:
    total = fs['total'].value
    attend = fs['attend'].value
    viewing = False
except KeyError:
    viewing = True

filename = 'rsvp.dat'
if not os.path.exists(filename):
    f = open(filename, 'w')
    f.write('{}')
    f.close()

json_data = open(filename)
j = json.load(json_data)
json_data.close()

if not viewing:
    found = False
    for key in j:
        if lastname in j[key].values() or firstname in j[key].values():
            found = True #stop repetetive dict addings

    if not found:
        j[time.time()] = {'lname': lastname, 'fname':firstname, 'total':total, 'attend':attend}

        f = open(filename, 'w')
        f.write(json.dumps(j))
        f.close()

stringer = ''

total_count = 0
wed_count = 0
rec_count = 0


for key in j:
    #date = time.ctime(float(key))
    #stringer += ('Submitted ' + str(date) + '<br />')
    stringer += ('First: ' + j[key]['fname'] + '<br />')
    stringer += ('Last: ' + j[key]['lname'] + '<br />')
    stringer += ('Party: ' + j[key]['total'] + '<br />')
    stringer += ('Attending: ' + j[key]['attend'] + '<br />')
    stringer += '<br /> <br />'

    #total_count += int(j[key]['total'])
    if j[key]['attend'] == 'Wedding and Reception':
        wed_count += int(j[key]['total'])
        rec_count += int(j[key]['total'])
    elif j[key]['attend'] == 'Wedding':
        wed_count += int(j[key]['total'])
    elif j[key]['attend'] == 'Reception':
        rec_count += int(j[key]['total'])

stringer = '<center><b>Wedding Total: {} &nbsp;&nbsp;&nbsp;Reception Total: {}</b> </center> <br /><br /> {}'.format(wed_count, rec_count, stringer)
body = '''

'''

html = """Content-type: text/html\n

<html>
    <style>{STYLE}</style>
	<head>
		<title>{TITLE}</title>
		{CSS}
		<script></script>
	</head>
<body>

<center>
<h2>Micah Page and Sondra Parkin Wedding RSVP list</h2>
</center>
{BODY}
</body>
</html>
""".format(
	TITLE = 'RSVP List',
	CSS = '', #css links
	BR = 'br />',
	COM = '<!--{}-->'.format(''),
	SPACE = '&nbsp;',
	TAB = '&nbsp;' * 4,
	QUOTE = '&quot;', #double quotes
	LEFT = '&lt;', #left carrot
	RIGHT = '&gt;', #right carrot
    STYLE=style_,
	BODY=stringer
	)




print(html)
