#!/usr/bin/env python

import cgi
import cgitb; cgitb.enable()
import sys

form = cgi.FieldStorage()

html = """Content-type: text/html\n

<html>
	<head>
		<title>{TITLE}</title>
		<script></script>
	</head>
<body>

<b>cgi.FieldStorage() is: <br></b>
{FORM} <br><br>

<form name="input" action="test.py" method="get">
Enter something: <input type="text" name="something">
<input type="submit" value="Submit">
</form>

</body>
</html>
""".format(
        TITLE='my title',
        FORM=form
	)
print(html)
