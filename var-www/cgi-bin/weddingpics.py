#!/usr/bin/env python3
import cgi, os
#import cgitb; cgitb.enable()



style_ = '''
body{
    background-image:url(../image/wed_pic.png);
    background-attachment:fixed;
    background-position:center;
    background-repeat:no-repeat;

    /*cover entire screen regardless of size of window*/
    -moz-background-size: cover;
    -webkit-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}
'''





def freespace(p):
	s = os.statvfs(p)
	return s.f_bsize * s.f_bavail

bytes_ = freespace('/')
KB = bytes_ / 1024
MB = (bytes_ / 1024) / 1024
GB = ((bytes_ / 1024) / 1024) / 1024






videotag = '''
<video  width="360" height="300" controls="controls">
  <source src="../wedding_photos/{filename}" type="video/{ext}">
  Sorry - Your browser does not support HTML5 video tag
</video>
<br /><br /> <br />
'''
video_ext = ['.mpg','.mp4', '.flv', '.avi', '.mts', '.mpeg', '.wmv', '.mov', 'divx', 'mpeg4','.ogg', '.ogm', '.ogv', '.swf', '.vob', '.qt', '.wma', '.wmx', '.ra', '.rm', '.ram', '.rmvb', '.3gp', '.mkv']






path = '/var/www/wedding_album'
#pic_list = []
pic_string = ''
filenum = len(os.listdir(path))

dicter = {}

for f in os.listdir(path):
	mode = int(os.stat(os.path.join(path, f)).st_ctime)
	dicter[f] = mode



if not os.listdir(path):
	pic_string = 'Nothing has been uploaded yet.'
#for f in os.listdir(path):
for f in sorted(dicter, key=dicter.get):
	if os.path.splitext(f)[1].lower() in video_ext:
		ext_ = os.path.splitext(f)[1][1:]
		filetag = '{}'.format(videotag.format(filename=f, ext=ext_))
	else:

		filetag = '<img style="max-width:750px;max-height-750px" src="http://www.metulburr.com/wedding_album/{}"><br /><br /><br />'.format(f)
	#pic_list.append(filetag)
	#pic_string += os.stat(f).st_mtime
	pic_string += filetag



html = '''Content-type: text/html\n

<html>
<title>Wedding Pictures</title>
<style>
{STYLE}
</style>
<body>
<b>Album Space:</b> GB:{gb:.1f} MB:{mb:.1f} <br />
<a href="http://www.metulburr.com/wedding.html">Return</a> <br />
<center>
<b>Videos using HTML5 video tag. Unable to view the videos?</b> Try the <a href="http://www.metulburr.com/wedding_album/">Index<a/>.
</center>
<center><h1>Micah and Sondra's Wedding Album</h1><center>
<center>

<b>Total:</b> {filenum}  </b><br /><br /><br />
{pics}
</center>
</body>
</html>
'''.format(pics=pic_string, filenum=filenum, gb=GB, mb=MB, STYLE=style_)

print(html)
